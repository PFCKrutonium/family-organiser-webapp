# Family Organiser Webapp

Webpage for organizing family plans, accessible on mobile and PC and displayed on a screen in the house using a Raspberry Pi.

## Features:
#### Frontend
- [ ] Homepage
- [ ] Calendar showing availability of family members for the day #1
	- [ ] Agenda for upcoming family events for current week

- [ ] Chores list featuring both automatic and manual chores assigned to people #4
- [ ] Indicator of who's in the house (based on phone MAC present on wifi) #3

- [ ] 'Summon' button with common reasons listed (e.g. dinner, get ready to leave, unload groceries)

#### Backend
- [ ] Get whether people are connected to the wifi #2
- [ ] A database for tasks, events, schedules, etc
- [ ] Notifications sent via text, email or Discord for upcoming events, summoning button, new chores, etc #3
