using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Linq;


namespace ResinBackend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RemoveFromCalendarController : ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            var Events =
                JsonConvert.DeserializeObject<List<CalendarEvent>>(System.IO.File.ReadAllText("./calendar.json"));

            string guid = "";
            foreach (var arg in Request.Query)
            {
                switch (arg.Key)
                {
                    case "guid":
                        guid = arg.Value;
                        break;
                }
            }

            var result = Events.FindIndex(s => s.GUID == guid);
            if (result == -1)
            {
                return "GUID not found";
            }
            else
            {
                Events.RemoveAt(result);
                System.IO.File.WriteAllText("./calendar.json",JsonConvert.SerializeObject(Events));
                return "Success";
            }
        }
    }
}