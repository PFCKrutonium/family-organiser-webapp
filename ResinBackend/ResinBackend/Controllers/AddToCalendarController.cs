using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using System.IO;
using Newtonsoft.Json;

namespace ResinBackend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AddToCalendarController : ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            var Cal = new CalendarEvent();
            Cal.Description = "None Provided";
            Cal.ShortDesc = "None";
            foreach (var arg in Request.Query)
            {
                switch (arg.Key)
                {
                    case "start":
                        Cal.Start = DateTime.Parse(arg.Value);
                        continue;
                    case "end":
                        Cal.End = DateTime.Parse(arg.Value);
                        continue;
                    case "description":
                        Cal.Description = arg.Value;
                        continue;
                    case "shortdesc":
                        Cal.ShortDesc = arg.Value;
                        continue;
                    case "familymemberid":
                        Cal.FamilyMemberID = arg.Value;
                        continue;
                }
            }

            if (Cal.Start.Date == DateTime.MinValue)
            {
                return "Missing Start Date";
            }

            if (Cal.End.Date == DateTime.MinValue)
            {
                return "Missing End Date";
            }

            if (Cal.FamilyMemberID == "" | Cal.FamilyMemberID == null)
            {
                return "Missing Family Member Name";
            }

            Cal.GUID = System.Guid.NewGuid().ToString();

            var Events =
                JsonConvert.DeserializeObject<List<CalendarEvent>>(System.IO.File.ReadAllText("./calendar.json"));
            Events.Add(Cal);
            foreach(var thing in Events)
            {
                if (thing.GUID == null)
                {
                    thing.GUID = System.Guid.NewGuid().ToString();
                }
            }
            Events.Sort((x,y) => DateTime.Compare(x.Start, y.Start));
            System.IO.File.WriteAllText("./calendar.json", JsonConvert.SerializeObject(Events, Formatting.Indented));
            return "Success";
        }
        static List<DateTime> SortDescending(List<DateTime> list)
        {
            list.Sort((a, b) => b.CompareTo(a));
            return list;
        }

    }
}