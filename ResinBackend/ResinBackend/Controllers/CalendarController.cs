using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.IO;
//using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc.Diagnostics;
using Newtonsoft.Json;
namespace ResinBackend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CalendarController : ControllerBase
    {

        [HttpGet]
        public string Get()
        {
            int Year = DateTime.Now.Year;
            int Month = DateTime.Now.Month;

            bool JustReturnEverything = false;
            foreach (var arg in Request.Query)
            {
                switch (arg.Key)
                {
                    case "year":
                        Year = Int32.Parse(arg.Value);
                        break;
                    case "month":
                        Month = Int32.Parse(arg.Value);
                        break;
                    case "all":
                        JustReturnEverything = true;
                        break;
                }
            }
            
            var jsonString = System.IO.File.ReadAllText("./calendar.json");
            //var jsonData = JsonConvert.DeserializeObject<CalendarEvent[]>(jsonString);
            var jsonData = JsonConvert.DeserializeObject<CalendarEvent[]>(jsonString);

            if (JustReturnEverything)
            {
                return JsonConvert.SerializeObject(jsonData, Formatting.Indented);
            }
            
            int day = 0;
            var Cal = new List<CalendarEvent>();
            
            Console.WriteLine("Year = " + Year);
            Console.WriteLine("Month = " + Month);
            
            while (day != DateTime.DaysInMonth(Year, Month))
            {
                day += 1;
                foreach (var calEvent in jsonData)
                {
                    if (calEvent.Start.Year == Year)
                    {
                        if (calEvent.Start.Month == Month)
                        {
                            System.Diagnostics.Debug.WriteLine(day);
                            if (calEvent.Start.Day == day)
                            {    
                                Cal.Add(calEvent);
                            }
                        }
                    }
                }
                
            }
            var jsonOut = JsonConvert.SerializeObject(Cal, Formatting.Indented);
            return jsonOut;
        }
    }
}
// var D = Request.Query;
// foreach (var Query in D)
// {
// //switch goes here
// }