using System;
using System.Collections;
using System.Collections.Generic;
namespace ResinBackend
{
    public class CalendarEvent
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string ShortDesc { get; set; }
        public string Description { get; set; }
        public string FamilyMemberID { get; set; }
        
        public string GUID { get; set; }
    }
}

